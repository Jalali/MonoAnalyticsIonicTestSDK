package cordova-plugin-monolytics;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ir.mono.monolyticsdk.Monolyitcs;

/**
 * This class echoes a string called from JavaScript.
 */
public class monolytics extends CordovaPlugin {

    private  final String InitializeActionName = "Initialize";
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals(InitializeActionName)){
            String sid = args.getString(0); //"509e61bb-f253-4be5-bccc-627907c1824b" ;
            String analyticId = args.getString(1); //"FEB90B7E-0B7F-487C-A90D-99DE2FD9D7B1" ;
            initialize(callbackContext, sid,analyticId);
        }
        return false;
    }

    private void initialize(String sid,String analyticId,CallbackContext callbackContext){
        Monolyitcs.init(callbackContext, analyticId, sid);
    }
}